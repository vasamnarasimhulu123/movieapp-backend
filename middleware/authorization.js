var jwt = require("jsonwebtoken");
require("dotenv").config();
let jwt_decode = require("jwt-decode");

// var decoded = jwt_decode(
//   "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMDlkYWM0YzktNTJlYy00NGYwLWEwOGEtNjEzMjEwY2IzZjM5IiwidXNlcl9uYW1lIjoib25lIiwidXNlcl9lbWFpbCI6Im9uZUBnbWFpbC5jb20iLCJpYXQiOjE2NDU1NTMyNDd9.HqZXWdgT0CxP8U5zVazu1b_3zQEMxU-AeFQd4Iu-dwM"
// );
// console.log(decoded, "gggggggggggggggggg");
// function authenticateToken(req, res, next) {
//   const authHeader = req.headers["authorization"];
//   const token = authHeader && authHeader.split(" ")[1];
//   console.log(token, "token console");
//   if (token == null) return res.status(401).json({ error: "token is null" });
//   jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (error, user) => {
//     if (error) return res.status(403).json({ error: error.message });
//     req.user = user;
//     next();
//   });
// }

// async function verifyToken(req, res, next) {
//   let token = req.headers.authorization;

//   try {
//     if (token) {
//       console.log(token, "token-id");

//       let payload = await jwt.verify(token, "my-secret".toString("base64"));

//       req.user = payload;
//       console.log(payload, "payload");
//       return next();
//     } else {
//       return res.status(401).json({ error: { body: ["Token Required"] } });
//     }
//   } catch (error) {
//     next(error);
//   }
// }

// function parseJwt(token) {
//   var base64Url = token.split(".")[1];
//   var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
//   var jsonPayload = decodeURIComponent(
//     atob(base64)
//       .split("")
//       .map(function (c) {
//         return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
//       })
//       .join("")
//   );

//   return JSON.parse(jsonPayload);
// }

module.exports = {
  verifyToken: async (req, res, next) => {
    let token = req.headers.authorization;

    try {
      if (token) {
        let payload = await jwt_decode(token);

        req.user = payload;

        console.log(payload, "payload");
        return next();
      } else {
        return res.status(401).json({ error: { body: ["Token Required"] } });
      }
    } catch (error) {
      next(error);
    }
  },
};
