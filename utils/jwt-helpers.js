const jwt = require("jsonwebtoken");
function jsonwebtoken({ user_id, user_name, user_email }) {
  const user = { user_id, user_name, user_email };
  const Token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
  // const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET);
  return { Token, user };
}
module.exports = jsonwebtoken;
