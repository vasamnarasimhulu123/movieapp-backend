CREATE DATABASE movie_database;

CREATE TABLE movies(
  movie_id SERIAL PRIMARY KEY,
  movie_name VARCHAR(200),
  movie_image VARCHAR(500),
  description VARCHAR(1000),
  director VARCHAR(200),
  users_id VARCHAR(2000)
);


CREATE EXTENSION "uuid-ossp";
CREATE EXTENSION "uuid-ossp";

CREATE TABLE users(
  user_id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  user_name TEXT NOT NULL,
  user_email TEXT NOT NULL,
  user_password TEXT NOT NULL
);

SELECT * FROM users;
INSERT INTO users (user_name,user_email,user_password) VALUES ('Bob','bob@gmail.com','bob');