const Pool = require("pg").Pool;
// const pool = new Pool({
//   user: "postgres",
//   password: "root",
//   database: "movie_database",
//   host: "localhost",
//   port: 5432,
// });

// module.exports = pool;
// pool.connect();
// pool.query(`SELECT * FROM movies`, (err, res) => {
//   if (!err) {
//     console.log(res.rows, "rows message");
//   } else {
//     console.log(err);
//   }
// });
const localPoolConfig = {
  user: "postgres",
  password: "root",
  database: "movie_database",
  host: "localhost",
  port: 5432,
};

const poolconfig = process.env.DATABASE_URL
  ? {
      connectionString: process.env.DATABASE_URL,
      ssl: { rejectUnauthorized: false },
    }
  : localPoolConfig;

const pool = new Pool(poolconfig);
module.exports = pool;
