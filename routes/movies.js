var express = require("express");
var router = express.Router();
var pool = require("../db");
const authorization = require("../middleware/authorization");

router.get("/", async (req, res, next) => {
  try {
    const allMovies = await pool.query("SELECT * FROM movies");
    res.json(allMovies.rows);
    // res.render("movie", { allMovies: allMovies.rows });
    next();
  } catch (err) {
    console.error(err.message);
  }
});

router.post("/", authorization.verifyToken, async (req, res) => {
  var users_id = req.user.user_id;
  var { movie_name, movie_image, description, director } = req.body;
  req.body.user_id = users_id;
  try {
    // "movie_name" : "movieName"
    // "movie_image": "movieImage",
    // "description" : "such a good movie all fans are eagerly waiting for that movie",
    // "director" : "Trivikram"
    const postMovie = await pool.query(
      "INSERT INTO movies(movie_name, movie_image, description, director, users_id) VALUES ($1, $2, $3, $4, $5) RETURNING *",
      [movie_name, movie_image, description, director, users_id]
    );
    console.log(postMovie.rows, "Post movie");
    res.json(postMovie.rows);
  } catch (err) {
    console.log(err.message);
  }
});

router.get("/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const SingleMovie = await pool.query(
      "SELECT * FROM movies WHERE movie_id = $1 ",
      [id]
    );
    res.json(SingleMovie.rows);
  } catch (err) {
    console.error(err.message);
  }
});

router.put("/:id", async (req, res) => {
  const { id } = req.params;
  const { movie_name, movie_image, description, director } = req.body;
  try {
    var updateMovie = await pool.query(
      "UPDATE movies SET movie_name = $1, movie_image=$2, description=$3, director=$4 WHERE movie_id = $5",
      [movie_name, movie_image, description, director, id]
    );

    res.json("successfully updated");
  } catch (err) {
    console.error(err.message);
  }
});

router.delete("/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const deleteMovie = await pool.query(
      "DELETE FROM movies WHERE movie_id = $1",
      [id]
    );
    res.json("Deleted Successfully");
  } catch (err) {
    console.error(err.nessage);
  }
});

module.exports = router;
