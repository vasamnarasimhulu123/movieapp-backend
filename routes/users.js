var express = require("express");
var router = express.Router();
var pool = require("../db");
var bcrypt = require("bcrypt");
var jsonwebtoken = require("../utils/jwt-helpers");
const { token } = require("morgan");
const verifyToken = require("../middleware/authorization");

/* GET users listing. */
router.get("/", verifyToken.verifyToken, async (req, res) => {
  let user_id = req.user.user_id;
  try {
    const allusers = await pool.query(
      "SELECT * FROM users WHERE user_id = $1",
      [user_id]
    );
    res.json(allusers.rows);
  } catch (err) {
    console.log(err.message);
  }
});

router.post("/register", async (req, res) => {
  var { user_name, user_email, user_password } = req.body;
  try {
    // {		"user_name": "vasam",
    //     "user_email": "vasam@chinna.com",
    //     "user_password": "vasam"}
    var salt = await bcrypt.genSalt(10);
    var bcryptPassword = bcrypt.hashSync(user_password, salt);
    // const hashedPassword = await bcrypt.hash(req.body.password, 10);
    const RegisterUser = await pool.query(
      "INSERT INTO users (user_name,user_email,user_password) VALUES ($1, $2, $3) RETURNING *",
      [user_name, user_email, bcryptPassword]
    );
    res.json({ users: RegisterUser.rows });
  } catch (err) {
    console.error(err.message);
  }
});

router.post("/login", async (req, res) => {
  const { user_email, user_password } = req.body;
  try {
    const users = await pool.query(
      "SELECT * FROM users WHERE user_email = $1",
      [user_email]
    );
    if (users?.rows?.length == 0)
      return res.status(401).json({ error: "Email is incorrect" });

    // password
    const validPassword = await bcrypt.compareSync(
      user_password,
      users.rows[0].user_password
    );
    if (!validPassword)
      return res.status(401).json({ error: "incorrect password" });
    // return res.status(200).json("success");
    // jwt
    // let users = users.rows;
    let tokens = jsonwebtoken(users.rows[0]);
    res.cookie("refresh_token", tokens.refreshToken, { httpOnly: true });
    res.json(tokens);
  } catch (err) {
    console.error(err.message);
  }
});

module.exports = router;
